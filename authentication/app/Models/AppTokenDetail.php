<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class AppTokenDetail extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table   = 'app_token_detail';
    protected $primaryKey   = 'app_token_detail_id';
    protected $hidden = ['pivot'];
    protected $guarded = ['app_token_detail_id'];
    protected $fillable = [
        'app_token', 'event_code','app_image','actived','deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password',
    // ];

    // public function apptoken(){
    //     return $this->belongsTo('App\Models\AppToken', 'foreign_key');
    // }
}
