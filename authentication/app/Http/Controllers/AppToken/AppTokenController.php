<?php

namespace App\Http\Controllers\AppToken;
use App\Http\Controllers\Controller;
use App\Models\AppToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
class AppTokenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index(){
        $apptoken = new AppToken();
        $data = $apptoken->newQuery();
        $data = $data->where('deleted','0')->with(['apptokendetail'])->get();
        //  $data = \App\Models\AppToken::all();
        if($data){
            return response()->json([
                'StatusCode'=>'200',
                'Error'=>false,
                'Message'=>'Sucess!',
                'Data'=>$data
                ],200);
        } else{
            return response()->json([
                'StatusCode'=>'404',
                'Error'=>true,
                'Message'=>'Not Found!',
                ],200);
        }
        
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'app_token' => 'required',
            'event_code' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>$validator->errors()->first()
            ],422);
        };
        $data = new \App\Models\AppToken;
        $data->app_token = $request->app_token;
        $data->event_code = $request->event_code;
        $data->app_image = $request->app_image;  
        $explodeParent = explode (';', $request ['app_image'] );
        $explodeTypeData = explode ('/', $explodeParent[0]);
        if ($explodeTypeData[0] == 'data:image'){
            $type = $explodeTypeData[1];
            if($type == 'svg+xml'){
                $type = explode('+',$type);
                $type = $type[0];
            }
            // $extension = pathinfo('Basepath', PATHINFO_EXTENSION);
            $inputArray['updated_data'] = date ('Y-m-d H:i:s');
            $image =  str_replace('data:image/'.$type.';base64,',',',$request['app_image']);
            $image = str_replace('','+',$image);
            $imageName = date('Ymd').'_'.rand(100000,1001238912).".".$type;
            //upload 
            file_put_contents(base_path('public/media/application').'/'.$imageName,base64_decode($image));
            //end upload
            $data['app_image'] = env('Basepath').'media/application'.$imageName;
        }
        $data->actived = $request->actived;
        $data->deleted = '0';
        $data->save();
        if($data){
            return response()->json([
                'StatusCode'=>'200',
                'Error'=>false,
                'Message'=>'Create Was Successfully!',                
                'Data'=>$data,
                ],200);
        } else{
            return response()->json([
                'StatusCode'=>'404',
                'Error'=>true,
                'Message'=>'Failed!',
                ],200);
        }
    }
    public function show($id){
        $data = \App\Models\AppToken::find($id);
        if($data){
            return response()->json([
                'StatusCode'=>'200',
                'Error'=>false,
                'Message'=>'Success!',
                'Data'=>$data,
                ],200);
        } else{
            return response()->json([
                'StatusCode'=>'404',
                'Error'=>true,
                'Message'=>'No Data Has Found!',
                ],200);
        }
    }
    public function update(Request $request,$id){
        $validator = Validator::make($request->all(),[
            'app_token' => 'required',
            'event_code' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>$validator->errors()->first()
            ],422);
        };
        $data = \App\Models\AppToken::findOrFail($id);
        $data->app_token = $request->app_token;
        $data->event_code = $request->event_code;
        $data->app_image = $request->app_image;
        $explodeParent = explode (';', $request ['app_image'] );
        $explodeTypeData = explode ('/', $explodeParent[0]);
        if ($explodeTypeData[0] == 'data:image'){
            $type = $explodeTypeData[1];
            if($type == 'svg+xml'){
                $type = explode('+',$type);
                $type = $type[0];
            }
            // $extension = pathinfo('Basepath', PATHINFO_EXTENSION);
            $inputArray['updated_data'] = date ('Y-m-d H:i:s');
            $image =  str_replace('data:image/'.$type.';base64,',',',$request['app_image']);
            $image = str_replace('','+',$image);
            $imageName = date('Ymd').'_'.rand(100000,1001238912).".".$type;
            //upload 
            file_put_contents(base_path('public/media/application').'/'.$imageName,base64_decode($image));
            //end upload
            $data['app_image'] = env('Basepath').'media/application'.$imageName;
        }
        $data->actived = $request->actived;
        $data->deleted = '0';
        $data->save();
        if($data){
            return response()->json([
                'StatusCode'=>'200',
                'Error'=>false,
                'Message'=>'Update Success!',
                'Data'=>$data
                ],200);
        } else{
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed To Update!',                
            ],200);
        }
    }
    public function destroy($id){
        $data = \App\Models\AppToken::findOrFail($id);
        if($data){
            $data->delete();
            return response()->json([
                'StatusCode'=>'200',
                'Error'=>false,
                'Message'=>'Data Deleted!',
                'Data'=>$data,
                ],200);
        } else{
            return response()->json([
                'StatusCode'=>'404',
                'Error'=>true,
                'Message'=>'Failed To Delete Data!',               
             ],200);
        }
    }

    //
}