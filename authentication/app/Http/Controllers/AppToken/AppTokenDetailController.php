<?php

namespace App\Http\Controllers\AppToken;
use App\Http\Controllers\Controller;
use App\Models\AppTokenDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
class AppTokenDetailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index(){
        $data = \App\Models\AppTokenDetail::all();
        if($data){
            return response()->json([
                'StatusCode'=>'200',
                'Error'=>false,
                'Message'=>'Succes!',
                'Data'=>$data 
                ],200);
        } else{
            return response()->json([
                'StatusCode'=>'404',
                'Error'=>true,
                'Message'=>'Not Found!',
                ],200);
        }
    }
    public function store(Request $request){
        
         $validator = Validator::make($request->all(),[
            'app_token_id' => 'required|integer',
            'security_key' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>$validator->errors()->first()
            ],422);
        };
        $data = new \App\Models\AppTokenDetail;
        $data->app_token_id = $request->app_token_id;
        $data->platform = $request->platform;
        $data->bundle_id = $request->bundle_id;  
        $data->security_key = $request->security_key;
        $data->save();
        if($data){
            return response()->json([
                'StatusCode'=>'200',
                'Error'=>false,
                'Message'=>'Create Was Successfully!',
                'Data'=>$data,
                ],200);
        } else{
            return response()->json([
                'StatusCode'=>'404',
                'Error'=>true,
                'Message'=>'Failed',
                ],200);
        }
    }
    public function show($id){
        $data = \App\Models\AppTokenDetail::find($id);
        if($data){
            return response()->json([
                'StatusCode'=>'200',
                'Error'=>false,
                'Message'=>'Success!',
                'Data'=>$data,
                ],200);
        } else{
            return response()->json([
                'StatusCode'=>'404',
                'Error'=>true,
                'Message'=>'No Data Has Found!',
                ],200);
        }
    }
    public function update(Request $request,$id){
        $validator = Validator::make($request->all(),[
            'app_token_id' => 'required|integer',
            'security_key' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>$validator->errors()->first()
            ],422);
        };
        $data = \App\Models\AppTokenDetail::findOrFail($id);
        $data->app_token_id = $request->app_token_id;
        $data->platform = $request->platform;
        $data->bundle_id = $request->bundle_id;  
        $data->security_key = $request->security_key;
        $data->save();
        if($data){
            return response()->json([
                'StatusCode'=>'200',
                'Error'=>false,
                'Message'=>'Update Success!',
                'Data'=>$data
                ],200);
        } else{
            return response()->json([
                'StatusCode'=>'404',
                'Error'=>true,
                'Message'=>'Failed To Update!',
                ],200);
        }
    }
    public function destroy($id){
        $data = \App\Models\AppTokenDetail::findOrFail($id);
        if($data){
            $data->delete();
            return response()->json([
                'StatusCode'=>'200',
                'Error'=>false,
                'Message'=>'Data Deleted!',
                'Data'=>$data,
                ],200);
        } else{
            return response()->json([
                'StatusCode'=>'404',
                'Error'=>true,
                'Message'=>'Failed To Delete Data!',
                ],200);
        }
    }

    //
}