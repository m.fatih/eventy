<?php

namespace App\Http\Controllers\Token;
use App\Http\Controllers\Controller;
use App\Models\AppToken;
use App\Models\AppTokenDetail;
use Illuminate\Http\Request;
use Validator;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

class RequestTokenController extends BaseController
{

     /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }
    /**
     * Create a new token.
     * 
     * @param  \App\Models\AppToken $user
     * @return string
     */
    protected function jwt($value) {
        // print_r($value); die();
        date_default_timezone_set('Asia/Jakarta');
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' =>  "subject",// Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60*24, // Expiration time
            'app_token_id' => $value->app_token_id,
            'app_token' => $value->app_token,
            'event_code' => $value->event_code,
            'app_image' => $value->app_image,
            'actived' => $value->deleted,
            'deleted' => $value->actived,
            'created_at' => $value->created_at,
            'updated_at' => $value->updated_at,
            'app_token_detail_id' => $value->app_token_detail_id,
            'platform' => $value->platform,
            'bundle_id' => $value->bundle_id,
            'security_key' => $value->security_key,
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    } 
    
    public function index(Request $request){
        if(empty($request->header('apptoken'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'App Token Not Exist!',
                ],200);
        }
        if(empty($request->header('securitykey'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Security Key Not Exist!',
                ],200); 
        }
        
        $app_token = $request->header('apptoken');
        $security_key = $request->header('securitykey');
        $getAppTokens = AppToken::join('app_token_detail', 'app_token.app_token_id', '=', 'app_token_detail.app_token_id')->where('app_token.app_token',$app_token)->where('app_token_detail.security_key',$security_key)->select('*')->first();
        if($getAppTokens){
            return response()->json([
                'StatusCode'=>'200',
                'Error' =>false,
                'Message' => 'Success!',
                'Data' => $this->jwt($getAppTokens)
            ], 200);
            // $this->jwt($getAppTokens);
            // print_r($this->jwt($getAppTokens));
        }
        else{
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Mesage' => 'Data Not Found!',
            ], 200); 
        }
    }
}