<?php
use \Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//Route APIController
// $router->get('/api/apptokens','AppToken\AppTokenController@index');
// $router->post('/api/apptokens','AppToken\AppTokenController@store');
// $router->get('/api/apptokens/{id}', 'AppToken\AppTokenController@show');
// $router->put('/api/apptokens/{id}','AppToken\AppTokenController@update');
// $router->delete('/api/apptokens/{id}','AppToken\AppTokenController@destroy');
//Route APIControllerDetail
// $router->get('/api/apptoken/details','AppToken\AppTokenDetailController@index');
// $router->post('/api/apptoken/details','AppToken\AppTokenDetailController@store');
// $router->get('/api/apptoken/details/{id}', 'AppToken\AppTokenDetailController@show');
// $router->put('/api/apptoken/details/{id}','AppToken\AppTokenDetailController@update');
// $router->delete('/api/apptoken/details/{id}','AppToken\AppTokenDetailController@destroy');
// $router->post(
//     'api/token', 
//     [
//        'uses' => 'Token\RequestTokenController@index'
//     ]
// );

// $router->post('/api/gettoken','Token\RequestTokenController@index');
// function(Request $request){
//     $token = app('auth')->attempt($request->only('app_token', 'security_key'));
//     return response()->json(compact('token'));
// });

$router->get('/', function () use ($router) {
    return $router->app->version();
    // return 'a';
});

$router->get('/key', function(){
    return str_random(32);
});

$router->group(['prefix' => 'api' ], function () use ($router) {
    $router->get('apptokens/{id}', 'AppToken\AppTokenController@show');
    $router->get('apptokens',  'AppToken\AppTokenController@index');
    $router->post('apptokens', 'AppToken\AppTokenController@store');
    $router->delete('apptokens/{id}', 'AppToken\AppTokenController@destroy');
    $router->put('apptokens/{id}',  'AppToken\AppTokenController@update');

    //app_token_detail
    $router->get('apptoken/details',  'AppToken\AppTokenDetailController@index');
    $router->get('apptoken/details/{id}', 'AppToken\AppTokenDetailController@show');
    $router->post('apptoken/details', 'AppToken\AppTokenDetailController@store');
    $router->delete('apptoken/details/{id}', 'AppToken\AppTokenDetailController@destroy');
    $router->put('apptoken/details/{id}',  'AppToken\AppTokenDetailController@update');
    
    //get token
    $router->post('token','Token\RequestTokenController@index');

  });

// $router->group(
//     ['middleware' => 'jwt.auth'], 
//     function() use ($router) {
//         $router->get('apptokens', function() {
//             $apptoken = \App\Models\AppToken::all();
//             return response()->json($apptoken);
//         });
//     }
// );
