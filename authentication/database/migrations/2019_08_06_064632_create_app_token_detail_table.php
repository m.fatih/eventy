<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTokenDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('app_token_detail')){
            Schema::table('app_token_detail',function($table){
                $tableName = 'app_token_detail';
                if (!Schema::hasColumn('app_token_detail','app_token_detail_id')){
                    $table->bigIncrements('app_token_detail_id');  
                }
                if (!Schema::hasColumn('app_token','app_token_id')){
                    $table->integer('app_token_id');  
                }
                if (!Schema::hasColumn('app_token','platform')){
                    $table->string('platform',100);  
                }
                if (!Schema::hasColumn('app_token','bundle_id')){
                    $table->string('bundle_id',100);  
                }
                if (!Schema::hasColumn('app_token','security_key')){
                    $table->string('security_key',100);  
                }
            });
        }else
        {
            Schema::create('app_token_detail', function (Blueprint $table) {
                $table->bigIncrements('app_token_detail_id');
                $table->integer('app_token_id');
                $table->string('platform',100);
                $table->string('bundle_id',100);
                $table->string('security_key',100);
                $table->timestamps();
            });
        }
    
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_token_detail');
    }
}
