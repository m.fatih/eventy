<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('app_token')){
            Schema::table('app_token',function($table){
                $tableName = 'app_token';
                if (!Schema::hasColumn('app_token','app_token_id')){
                    $table->bigIncrements('app_token_id');  
                }
                if (Schema::hasColumn('app_token','app_token')){
                    if(!collect(DB::select("SHOW INDEXES FROM app_token"))->pluck('Key_name')->contains($tableName.'_app_token_index')){
                        $table->index('app_token');
                    } 
                 }
                 else{
                    $table->string('app_token',100)->index();
                 }
                 if (Schema::hasColumn('app_token','event_code')){
                    if(!collect(DB::select("SHOW INDEXES FROM event_code"))->pluck('Key_name')->contains($tableName.'_event_code_index')){
                        $table->index('app_token');
                    } 
                 }
                 else{
                    $table->string('event_code',100)->index();
                 }
                if (!Schema::hasColumn('app_token','app_image')){
                    $table->text('app_image');  
                }
                if (!Schema::hasColumn('app_token','actived')){
                    $table->enum('actived',['0','1'])->default('0');
                }
                if (!Schema::hasColumn('app_token','deleted')){
                    $table->enum('deleted',['0','1'])->default('0');
                }
            });
        }else
        {
            Schema::create('app_token', function (Blueprint $table) {
                $table->bigIncrements('app_token_id');
                $table->string('app_token',100)->index();
                $table->string('event_code',100)->index();
                $table->text('app_image');
                $table->enum('actived',['0','1'])->default('0');
                $table->enum('deleted',['0','1'])->default('0');
                $table->timestamps();
            });
        }
    
        }
       
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_token');
    }
}
