<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::post('cms-login','Auth\AuthController@login');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin','middleware' => 'admin'],  function () {
    //, 'middleware' => ['auth', 'role:admin']
    Route::get('/', function(){
        return view('admin.index');
    });

    //splash_screen
    Route::resource('splash_screen', 'General\SplashScreenController')->except(['create','show','edit']);
    Route::get('splash_screen/activate/{splash_id}', 'General\SplashScreenController@activate');

    //banner
    Route::resource('banner', 'General\BannerController')->except(['create','show','edit']);
    Route::get('banner/activate/{banner_id}', 'General\BannerController@activate');

    Route::resource('news', 'Product\NewsController');
    
    Route::resource('event', 'Product\EventController');

    Route::get('member', function(){
        return view('admin.member.index');
    });
    // Route::get('/', 'AdminController@check');
    // Route::post('/regist_company','CompanyController@store');
    // Route::get('/company_profile)','CompanyController@getComp')->name('company_profile');
    // Route::put('updateLogo/{id}','CompanyController@updateLogo');
    // Route::put('update/{id}','CompanyController@update');
    // Route::resource('article', 'ArticleController');
    // Route::get('add_article','ArticleController@formAdd');
    // Route::get('edit_article/{id}','ArticleController@edit');
    // Route::resource('category', 'CategoryController');
    // Route::resource('property', 'PropertyController');
    // Route::resource('property_detail','DetailController');
    });

//superadmin
Route::group(['prefix' => 'master','middleware' => 'admin'],  function () {
    //, 'middleware' => ['auth', 'role:admin']
    Route::get('/', 'Admin\DashboardController@master');
});
