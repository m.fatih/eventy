@extends('layouts.template')

@section('cms')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    BREADCRUMB
    <div class="card">
        <div class="card-header card-header-info">
            <h4 class="card-title">Add News / Article</h4>
            <p class="card-category">Complete the form below to submit news</p>
        </div>
        <div class="card-body">
            <form method="POST" action="{{route('news.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <div class="col-md-4 col-sm-4 col-xs-4" style="text-align:right;padding:10px;margin-top:5px">
                        <label>News / Article Title</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <input type="text" class="form-control" name="news_title">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4 col-sm-4 col-xs-4" style="text-align:right;padding:10px;margin-top:5px">
                        <label>News / Article Creator </label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <input type="text" class="form-control" name="news_creator">
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-4 col-sm-4 col-xs-4" style="text-align:right;padding:10px;margin-top:5px">
                        <label>News Image/Thumbnail File</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8" style="margin-top:10px">
                        <input type="file" id="inputBanner" name="news_image">
                    </div>
                </div>

                <div class="banner">
                    <img id="banner" width="100%" height="100%">
                </div>

                <label style="margin-top:30px;font-size:24px">Content : </label>
                <textarea style="width:100%" rows="20" name="news_desc" id="content"></textarea>
                <div style="text-align:right;">
                    <a class="navbar-brand" href="{{url()->previous()}}">
                        <button type="button" class="btn btn-danger">Cancel </button></a>
                    <button class="btn btn-primary" type="submit">Save </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#banner').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputBanner").change(function () {
        readURL(this);
    });

    //$('#content').wysiwyg();

    

</script>
@endsection
