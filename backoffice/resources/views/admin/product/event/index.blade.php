@extends('layouts.template')

@section('cms')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content " style="padding:30px;">
    <a href="{{route('news.create')}}"><button type="button" class="btn"
                style="background-color:#00BCD4;border-radius:7px!important;color:white;margin-bottom:20px">
                <i class="fa fa-plus" style="margin-right:15px;"></i>Tambah
                Artikel</button></a>
        <div class="table-responsive">
            <table id="example" class="table table-article table-striped table-bordered" style="width:100%">
                <thead>
                    <tr style="text-align:center">
                        <th>No</th>
                        <th>Judul</th>
                        <th>Penulis</th>
                        <th>Tanggal Penulisan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $item)
                    <tr>
                        <td style="text-align:center">{{++$key}}</td>
                        <td style="width:25%">{{$item['event_title']}}</td>
                        <td style="text-align:center;width:20%">{{$item['news_creator']}}</td>
                        <td style="text-align:right;width:15%">{{date('d F Y', strtotime($item['created_at']))}}</td>
                        <td style="text-align:center;">
                                <button class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Actions
    
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" data-toggle="modal"
                                            data-target=""><i class="fa fa-info"
                                                style="margin-left:10px;margin-right:30px"></i>Edit</a>
    
                                        <div class="dropdown-item">
                                            <form action="" method="POST">
                                                {{csrf_field()}} {{method_field('DELETE')}}
                                                <button type="button" style="width:100%;height:100%;background-color: Transparent;
                                                background-repeat:no-repeat;
                                                border: none;
                                                cursor:pointer;
                                                overflow: hidden;
                                                outline:none;"
                                                    onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ?')){return this.form.submit()}else{return false}"><i
                                                        class="fa fa-trash" style="margin-right:20px;"></i>Delete</button>
    
                                            </form>
                                        </div>
                                    </div>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true,
            "paging": true,
            "ordering": true,
            "searching": true,
            "info": false,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": false,
        });
    });

</script>
@endsection
