@extends('layouts.template')

@section('cms')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content " style="padding:30px;">
        <a href=""><button type="button" class="btn"
                style="background-color:#5cb85c;border-radius:7px!important;color:white;margin-bottom:20px">
                <i class="fa fa-plus" style="margin-right:15px;"></i>Tambah
                Artikel</button></a>
        <div class="table-responsive">
            <table id="example" class="table table-article table-striped table-bordered" style="width:100%">
                <thead>
                    <tr style="text-align:center">
                        <th>No</th>
                        <th>Judul</th>
                        <th>Penulis</th>
                        <th>Tanggal Penulisan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- @foreach ($article as $key => $item)
                    <tr>
                        <td style="text-align:center">{{++$key}}</td>
                        <td style="width:25%">{{$item->title}}</td>
                        <td style="text-align:center;width:20%">{{$item->author}}</td>
                        <td style="text-align:right;width:15%">{{date('d M Y', strtotime($item->created_at))}}</td>
                        <td ><div style="text-align:center;margin-left:10px"><a href="{{url('admin/edit_article/'.$item->id)}}">
                            <button class="btn btn-info"
                                        style="border-radius:5px!important;float:left"><i class="fa fa-info"
                                            style="margin-right:20px;"></i>Edit</button>
                                        </a><form action="{{url('admin/article/'.$item->id)}}" method="POST">
                                    {{csrf_field()}} {{method_field('DELETE')}}<button type="button"
                                        class="btn btn-danger" style="border-radius:5px!important"
                                        onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ?')){return this.form.submit()}else{return false}"><i
                                            class="fa fa-trash" style="margin-right:20px;"></i>Delete</button></form>
                                        </div>
                                
                        </td>
                    </tr>
                    @endforeach --}}
                </tbody>
            </table>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true,
            "paging": true,
            "ordering": true,
            "searching": true,
            "info": false,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": false,
        });
    });

</script>
@endsection
