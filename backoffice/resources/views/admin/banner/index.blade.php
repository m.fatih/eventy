@extends('layouts.template')

@section('cms')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="card">
        <a data-toggle="modal" data-target="#addBanner" style="cursor: pointer;">
            <div class="card-header card-header-info">
                <h4 style="margin-left:30px" class="card-title">
                    <i class="fa fa-plus" style="margin-right:15px;"></i>Tambah
                    Banner</h4>
            </div>
        </a>
        <div class="card-body">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content " style="padding:30px;">

                <div class="table-responsive">
                    <table id="example" class="table table-article table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr style="text-align:center">
                                <th>No</th>
                                <th>Event Code</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Activation</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $key => $item)
                            <tr>
                                <td style="text-align:center">{{++$key}}</td>
                                <td style="text-align:right;width:15%">{{$item['event_code']}}</td>
                                <td style="text-align:center;width:20%">{{$item['banner_name']}}</td>
                                <td style="width:15%">
                                    <div style="height:100px;width:100%;">
                                        <img src="{{$banner_image[--$key]}}" style="width:100%;height:100%;">
                                    </div>
                                </td>
                                <td style="text-align:right;width:20%">{{$item['banner_desc']}}</td>
                                <td style="text-align:center;">
                                    <form action="{{url('admin/banner/activate',$item['banner_id'])}}"
                                        method="POST">
                                        @csrf {{method_field('GET')}}
                                        <label class="switch-act">
                                            <input type="checkbox" onclick="this.form.submit()"
                                                @if($item['status']=='Active' ){{'checked'}}@endif>
                                            <span class="slider-act"></span>
                                        </label>
                                    </form>
                                </td>
                                <td>
                                    <div style="text-align:center;margin-left:10px">
                                        <a data-toggle="modal"
                                            data-target="#editBanner{{$item['banner_id']}}"><button
                                                class="btn btn-info" style="border-radius:5px!important;"><i class="fa fa-info"
                                                        style="margin-left:0px;margin-right:40px"></i>Edit</button></a>
                                        <form action="{{route('banner.destroy', $item['banner_id'])}}" method="POST" >
                                            {{csrf_field()}} {{method_field('DELETE')}}<button type="button"
                                                class="btn btn-danger" style="border-radius:5px!important"
                                                onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ?')){return this.form.submit()}else{return false}"><i
                                                    class="fa fa-trash" style="margin-right:15px"></i>Delete</button>
                                        </form>
                                    </div>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<div id="addBanner" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="width:100%;">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Banner</h4>
            </div>
            <div class="modal-body">
                <form role="form" style="padding-left:30px;padding-right:30px;padding-top:30px;padding-bot:0px;"
                    method="POST" action="{{route('banner.store')}}" enctype="multipart/form-data">
                    @csrf{{method_field('POST')}}
                    <div class="form-body">
                        <div class="form-group row" style="padding-bottom:5px;">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                                <label>Event Code</label>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                                <input type="text" class="form-control" name="event_code">

                            </div>
                        </div>
                        <div class="form-group row" style="padding-bottom:5px;">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                                <label>Name</label>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                                <input type="text" class="form-control" name="banner_name">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12" style="padding:15px;">
                                <label>Banner Image</label>

                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12 " style="padding:15px;">
                                <input type="file" id="inputBanner" name="banner_image">
                            </div>

                        </div>
                        <div class="ban-form" style="margin-bottom:30px;">
                            <img id="banner" width="100%" height="100%">
                        </div>
                        <div class="form-group row" style="padding-bottom:5px;">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                                <label>Description</label>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                                <input type="text" class="form-control" name="banner_desc">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="margin-top:20px;padding-right:0px">
                        <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
</div>

@foreach ($data as $key =>$item)
<div id="editBanner{{$item['banner_id']}}" class="modal fade" role="dialog">
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width:100%;">
        <div class="modal-header">
            <h4 class="modal-title">Edit Data Banner</h4>
        </div>
        <div class="modal-body">
            <form role="form" style="padding-left:30px;padding-right:30px;padding-top:30px;padding-bot:0px;"
                method="POST" action="{{route('banner.update', $item['banner_id'])}}" enctype="multipart/form-data">
                @csrf{{method_field('PUT')}}
                <div class="form-body">
                        {{-- <div class="form-group row" style="padding-bottom:5px;">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                                <label>Event Code</label>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                            <input type="text" class="form-control" name="event_code" value="{{$item['event_code']}}">

                            </div>
                        </div> --}}
                        <div class="form-group row" style="padding-bottom:5px;">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                                <label>Title</label>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                                <input type="text" class="form-control" name="banner_name" value="{{$item['banner_name']}}"> 

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12" style="padding:15px;">
                                <label>Banner Image</label>

                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12 " style="padding:15px;">
                                <input type="file" id="inputBanner2" name="banner_image" >
                            </div>

                        </div>
                        <div class="image-form" style="margin-bottom:30px;">
                            <img src="{{$banner_image[$key]}}" id="ban2" width="100%" height="100%">
                        </div>
                        <div class="form-group row" style="padding-bottom:5px;">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                                <label>Description</label>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                                <input type="text" class="form-control" name="banner_desc" value="{{$item['banner_desc']}}">

                            </div>
                        </div>
                    </div>
                <div class="modal-footer" style="margin-top:20px;padding-right:0px">
                    <button type="button" data-dismiss="modal" class="btn btn-outline-danger">Cancel</button>
                    <button type="submit" class="btn btn-outline-info">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->
</div>
@endforeach
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true,
            "paging": true,
            "ordering": true,
            "searching": true,
            "info": false,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": false,
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#banner').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputBanner").change(function () {
        readURL(this);
    });

    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#ban2').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputBanner2").change(function () {
        readURL2(this);
    });

</script>
@endsection
