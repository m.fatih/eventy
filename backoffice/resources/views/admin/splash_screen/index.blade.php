@extends('layouts.template')

@section('cms')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="card">
        <a data-toggle="modal" data-target="#addSplash" style="cursor: pointer;">
            <div class="card-header card-header-info">
                <h4 style="margin-left:30px" class="card-title">
                    <i class="fa fa-plus" style="margin-right:15px;"></i>Tambah
                    Splash Screen</h4>
            </div>
        </a>
        <div class="card-body">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content " style="padding:30px;">

                <div class="table-responsive">
                    <table id="example" class="table table-article table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr style="text-align:center">
                                <th>No</th>
                                <th>Event Code</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>BG Color</th>
                                <th>Activation</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $key => $item)
                            <tr>
                                <td style="text-align:center">{{++$key}}</td>
                                <td style="text-align:right;width:15%">{{$item['event_code']}}</td>
                                <td style="text-align:center;width:20%">{{$item['splash_title']}}</td>
                                <td style="width:15%">
                                    <div style="height:100px;width:100%;">
                                        <img src="{{$splash[--$key]}}" style="width:100%;height:100%;">
                                    </div>
                                </td>
                                <td style="text-align:right;width:20%">{{$item['splash_desc']}}</td>
                                <td style="text-align:right;width:15%">{{$item['splash_bg_color']}}</td>
                                <td style="text-align:center;">
                                    <form action="{{url('admin/splash_screen/activate',$item['splash_id'])}}"
                                        method="POST">
                                        @csrf {{method_field('GET')}}
                                        <label class="switch-act">
                                            <input type="checkbox" onclick="this.form.submit()"
                                                @if($item['status']=='Active' ){{'checked'}}@endif>
                                            <span class="slider-act"></span>
                                        </label>
                                    </form>
                                </td>
                                <td>
                                    <div style="text-align:center;margin-left:10px">
                                        <a data-toggle="modal" data-target="#editSplash{{$item['splash_id']}}"><button
                                                class="btn btn-info" style="border-radius:5px!important;"><i
                                                    class="fa fa-info"
                                                    style="margin-left:0px;margin-right:40px"></i>Edit</button></a>
                                        <form action="{{route('splash_screen.destroy', $item['splash_id'])}}"
                                            method="POST">
                                            {{csrf_field()}} {{method_field('DELETE')}}<button type="button"
                                                class="btn btn-danger" style="border-radius:5px!important"
                                                onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ?')){return this.form.submit()}else{return false}"><i
                                                    class="fa fa-trash" style="margin-right:15px"></i>Delete</button>
                                        </form>
                                    </div>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<div id="addSplash" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="width:100%;">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Splash Screen</h4>
            </div>
            <div class="modal-body">
                <form role="form" style="padding-left:30px;padding-right:30px;padding-top:30px;padding-bot:0px;"
                    method="POST" action="{{route('splash_screen.store')}}" enctype="multipart/form-data">
                    @csrf{{method_field('POST')}}
                    <div class="form-body">
                        <div class="form-group row" style="padding-bottom:5px;">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                                <label>Event Code</label>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                                <input type="text" class="form-control" name="event_code">

                            </div>
                        </div>
                        <div class="form-group row" style="padding-bottom:5px;">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                                <label>Title</label>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                                <input type="text" class="form-control" name="splash_title">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12" style="padding:15px;">
                                <label>Splash Image</label>

                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12 " style="padding:15px;">
                                <input type="file" id="inputSS" name="splash_image">
                            </div>

                        </div>
                        <div class="image-form" style="margin-bottom:30px;">
                            <img id="ss" width="100%" height="100%">
                        </div>
                        <div class="form-group row" style="padding-bottom:5px;">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                                <label>Background Color</label>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                                <input type="color" name="splash_bg_color">

                            </div>
                        </div>
                        <div class="form-group row" style="padding-bottom:5px;">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                                <label>Description</label>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                                <input type="text" class="form-control" name="splash_desc">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="margin-top:20px;padding-right:0px">
                        <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
</div>

@foreach ($data as $key =>$item)
<div id="editSplash{{$item['splash_id']}}" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="width:100%;">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data Splash Screen</h4>
            </div>
            <div class="modal-body">
                <form role="form" style="padding-left:30px;padding-right:30px;padding-top:30px;padding-bot:0px;"
                    method="POST" action="{{route('splash_screen.update', $item['splash_id'])}}"
                    enctype="multipart/form-data">
                    @csrf{{method_field('PUT')}}
                    <div class="form-body">
                        {{-- <div class="form-group row" style="padding-bottom:5px;">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                                <label>Event Code</label>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                            <input type="text" class="form-control" name="event_code" value="{{$item['event_code']}}">

                    </div>
            </div> --}}
            <div class="form-group row" style="padding-bottom:5px;">
                <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                    <label>Title</label>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                    <input type="text" class="form-control" name="splash_title" value="{{$item['splash_title']}}">

                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12" style="padding:15px;">
                    <label>Splash Image</label>

                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 " style="padding:15px;">
                    <input type="file" id="inputSS2" name="splash_image">
                </div>

            </div>
            <div class="image-form" style="margin-bottom:30px;">
                <img src="{{$splash[$key]}}" id="ss2" width="100%" height="100%">
            </div>
            <div class="form-group row" style="padding-bottom:5px;">
                <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                    <label>Background Color</label>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                    <input type="color" name="splash_bg_color" value="{{$item['splash_bg_color']}}"
                        style="margin-top:10px;">

                </div>
            </div>
            <div class="form-group row" style="padding-bottom:5px;">
                <div class="col-md-4 col-sm-4 col-xs-4" style="padding:0px;margin-top:5px">
                    <label>Description</label>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8" style="padding:0px;">
                    <input type="text" class="form-control" name="splash_desc" value="{{$item['splash_desc']}}">

                </div>
            </div>
        </div>
        <div class="modal-footer" style="margin-top:20px;padding-right:0px">
            <button type="button" data-dismiss="modal" class="btn btn-outline-danger">Cancel</button>
            <button type="submit" class="btn btn-outline-info">Save</button>
        </div>
        </form>
    </div>
</div>
</div>
<!-- END SAMPLE FORM PORTLET-->
</div>
@endforeach
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            responsive: true,
            "paging": true,
            "ordering": true,
            "searching": true,
            "info": false,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": false,
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#ss').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputSS").change(function () {
        readURL(this);
    });

    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#ss2').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputSS2").change(function () {
        readURL2(this);
    });

</script>
@endsection
