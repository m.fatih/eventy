<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Redirect;
use Auth;
use Carbon\Carbon;
use Guzzle\Http\Message\Response;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;


class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new Client();

        //dd($client);
        $url = config('constants.url.gateway').'news';
        $token = Session::get('app_token');
        //dd($token);
       
        $header = ['token' => $token];
        //dd($header);
        $news = $client->request('GET',$url,[
                        'headers' => $header,
                    ]);
		$news = json_decode($news->getBody()->getContents(),true);
        //dd($news);
        $data = $news['Data'];
        //dd($data);
        $news=[];
        $news_image=[];
        $i = 0;
        foreach($data as $dt){
            $news[] = $dt['news_image'];
        }
        foreach($news as $var){
            $news_image[] = $var;
        }
        $count = count($news_image);
        //dd($data[0]['event_code'],$data[1]['event_code']);
        //dd($sp);
        
        return view('admin.product.news.index',compact('data','news_image','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.news.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new Client();

        $url = config('constants.url.gateway').'/addnews';
        $token = Session::get('app_token');
        $header = ['token' => $token];

        $decodeToken = JWT::decode($token,env('JWT_SECRET'), array('HS256'));
        //return response()->json($decodeToken->event_code);

        $event_code = $decodeToken->event_code;

        $file=$request->file('news_image');
        //$dt = Carbon::now();
        $fileName = str_random(8).'-'.$file->getClientOriginalName();
        $path = public_path('image/News/');
        $file->move($path, $fileName);
        //dd($fileName);  


        $responseData = $client->request('POST',$url.'/'.$event_code,[
                        'headers' => $header,
                        'multipart' =>  [
                            [	'name' => 'event_code',
                                'contents' => $request->event_code,				
                            ],
                            [	'name' => 'splash_title',
                                'contents' => $request->splash_title
                            ],
                            [	'name' => 'splash_desc',
                                'contents' => $request->splash_desc
                            ],
                            [
                                'name' => 'splash_image',
                                'contents' => fopen($path. $fileName , 'r')
                            ],
                            [	'name' => 'splash_bg_color',
                                'contents' => $request->splash_bg_color
                            ]
                        ]
                        ]);
        $responseData = json_decode($responseData->getBody()->getContents(),true);
        if(file_exists($path.$fileName)) unlink($path.$fileName);
        //dd($responseData);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
