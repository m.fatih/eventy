<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Redirect;
use Auth;
use Carbon\Carbon;
use Guzzle\Http\Message\Response;

class SplashScreenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new Client();

        $url = config('constants.url.gateway').'splashscreen';
        $token = Session::get('app_token');
        $header = ['token' => $token];

        $splash = $client->request('GET',$url,[
                        'headers' => $header,
                    ]);
		$splash = json_decode($splash->getBody()->getContents(),true);
        //dd($splash);
        $data = $splash['Data'];
        //dd($data);
        $sp=[];
        $splash=[];
        $i = 0;
        foreach($data as $dt){
            $sp[] = $dt['splash_image'];
        }
        foreach($sp as $var){
            $splash[] = $var;
        }
        //dd($splash);
        //dd(count($sp));
        $count = count($splash);
        //dd($data[0]['event_code'],$data[1]['event_code']);
        //dd($sp);
        
        return view('admin.splash_screen.index',compact('data','splash','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new Client();

        $url = config('constants.url.gateway').'/addsplashscreen';
        $token = Session::get('app_token');
        $header = ['token' => $token];

        $file=$request->file('splash_image');
        //$dt = Carbon::now();
        $fileName = str_random(8).'-'.$file->getClientOriginalName();
        $path = public_path('image/SplashScreen/');
        $file->move($path, $fileName);
        //dd($fileName);  


        $responseData = $client->request('POST',$url,[
                        'headers' => $header,
                        'multipart' =>  [
                            [	'name' => 'event_code',
                                'contents' => $request->event_code,				
                            ],
                            [	'name' => 'splash_title',
                                'contents' => $request->splash_title
                            ],
                            [	'name' => 'splash_desc',
                                'contents' => $request->splash_desc
                            ],
                            [
                                'name' => 'splash_image',
                                'contents' => fopen($path. $fileName , 'r')
                            ],
                            [	'name' => 'splash_bg_color',
                                'contents' => $request->splash_bg_color
                            ]
                        ]
                        ]);
        $responseData = json_decode($responseData->getBody()->getContents(),true);
        if(file_exists($path.$fileName)) unlink($path.$fileName);
        //dd($responseData);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = new Client();
        $url = config('constants.url.gateway').'/editsplash';
        $token = Session::get('app_token');
        $header = ['token' => $token];

        if($request->splash_image != null){
           
            //return 'not null';
            $file=$request->file('splash_image');
            //$dt = Carbon::now();
            $fileName = str_random(8).'-'.$file->getClientOriginalName();
            $path = public_path('image/SplashScreen/');
            $file->move($path, $fileName);
            //dd($fileName);  

            $responseData = $client->request('POST',$url.'/'.$id,[
                            'headers' => $header,
                            'multipart' =>  [
                                [	'name' => 'splash_title',
                                    'contents' => $request->splash_title
                                ],
                                [	'name' => 'splash_desc',
                                    'contents' => $request->splash_desc
                                ],
                                [
                                    'name' => 'splash_image',
                                    'contents' => fopen($path. $fileName , 'r')
                                ],
                                [	'name' => 'splash_bg_color',
                                    'contents' => $request->splash_bg_color
                                ]
                            ]
                            ]);
            $responseData = json_decode($responseData->getBody()->getContents(),true);
            if(file_exists($path.$fileName)) unlink($path.$fileName);
            return back();
        }else{
            //return 'ok';
            $responseData = $client->request('POST',$url.'/'.$id,[
                'headers' => $header,
                'multipart' =>  [
                    [	'name' => 'splash_title',
                        'contents' => $request->splash_title
                    ],
                    [	'name' => 'splash_desc',
                        'contents' => $request->splash_desc
                    ],
                    [
                        'name' => 'splash_image',
                        'contents' => null
                    ],
                    [	'name' => 'splash_bg_color',
                        'contents' => $request->splash_bg_color
                    ]
                ]
                ]);
            $responseData = json_decode($responseData->getBody()->getContents(),true);
            //return 'ok';
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = new Client();

        $url = config('constants.url.gateway').'/deletesplash';
        $token = Session::get('app_token');
        $header = ['token' => $token];

        $responseData = $client->request('DELETE',$url.'/'.$id,[
                        'headers' => $header,
                        ]);
        return back();
    }

    public function activate($id){
        $client = new Client();
        $url = config('constants.url.gateway').'/activationsplash';
        $token = Session::get('app_token');
        $header = ['token' => $token];
		
		$splash = $client->request('GET',$url.'/'.$id,[
            'headers' => $header,
        ]);
		return back();
	
    }
}
