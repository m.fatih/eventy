<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Redirect;
use Auth;
use Carbon\Carbon;
use Guzzle\Http\Message\Response;


class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new Client();

        $url = config('constants.url.gateway').'banner';
        $token = Session::get('app_token');
        $header = ['token' => $token];

        $banner = $client->request('GET',$url,[
                        'headers' => $header,
                    ]);
		$banner = json_decode($banner->getBody()->getContents(),true);
        //dd($banner);
        $data = $banner['Data'];
        //dd($data);
        $banner=[];
        $banner_image=[];
        $i = 0;
        foreach($data as $dt){
            $banner[] = $dt['banner_image'];
        }
        foreach($banner as $var){
            $banner_image[] = $var;
        }
        $count = count($banner_image);
        //dd($data[0]['event_code'],$data[1]['event_code']);
        //dd($sp);
        
        return view('admin.banner.index',compact('data','banner_image','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new Client();

        $url = config('constants.url.gateway').'/addbanner';
        $token = Session::get('app_token');
        $header = ['token' => $token];

        $file=$request->file('banner_image');
        //$dt = Carbon::now();
        $fileName = str_random(8).'-'.$file->getClientOriginalName();
        $path = public_path('image/Banner/');
        $file->move($path, $fileName);
        //dd($fileName);  


        $responseData = $client->request('POST',$url,[
                        'headers' => $header,
                        'multipart' =>  [
                            [	'name' => 'event_code',
                                'contents' => $request->event_code,				
                            ],
                            [	'name' => 'banner_name',
                                'contents' => $request->banner_name
                            ],
                            [	'name' => 'banner_desc',
                                'contents' => $request->banner_desc
                            ],
                            [
                                'name' => 'banner_image',
                                'contents' => fopen($path. $fileName , 'r')
                            ],
                        ]
                        ]);
        $responseData = json_decode($responseData->getBody()->getContents(),true);
        if(file_exists($path.$fileName)) unlink($path.$fileName);
        //dd($responseData);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = new Client();

        $url = config('constants.url.gateway').'/editbanner';
        $token = Session::get('app_token');
        $header = ['token' => $token];

        if($request->banner_image != null){
           
            //return 'not null';
            $file=$request->file('banner_image');
            //$dt = Carbon::now();
            $fileName = str_random(8).'-'.$file->getClientOriginalName();
            $path = public_path('image/Banner/');
            $file->move($path, $fileName);
            //dd($fileName);  

            $responseData = $client->request('POST',$url.'/'.$id,[
                            'headers' => $header,
                            'multipart' =>  [
                                [	'name' => 'banner_name',
                                    'contents' => $request->banner_name
                                ],
                                [	'name' => 'banner_desc',
                                    'contents' => $request->banner_desc
                                ],
                                [
                                    'name' => 'banner_image',
                                    'contents' => fopen($path. $fileName , 'r')
                                ],
                            ]
                            ]);
            $responseData = json_decode($responseData->getBody()->getContents(),true);
            if(file_exists($path.$fileName)) unlink($path.$fileName);
            return back();
        }else{
            //return 'ok';
            $responseData = $client->request('POST',$url.'/'.$id,[
                'headers' => $header,
                'multipart' =>  [
                    [	'name' => 'banner_name',
                        'contents' => $request->banner_name
                    ],
                    [	'name' => 'banner_desc',
                        'contents' => $request->banner_desc
                    ],
                    [
                        'name' => 'banner_image',
                        'contents' => null
                    ],
                ]
                ]);
            $responseData = json_decode($responseData->getBody()->getContents(),true);
            //return 'ok';
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = new Client();

        $url = config('constants.url.gateway').'/deletebanner';
        $token = Session::get('app_token');
        $header = ['token' => $token];

        $responseData = $client->request('DELETE',$url.'/'.$id,[
                        'headers' => $header,
                        ]);
        $responseData = json_decode($responseData->getBody()->getContents(),true);
        //if(file_exists($path.$fileName)) unlink($path.$fileName);
        //dd($responseData);
        return back();
    }

    public function activate($id){
        $client = new Client();
        $url = config('constants.url.gateway').'/activationbanner';
        $token = Session::get('app_token');
        $header = ['token' => $token];
		
		$splash = $client->request('GET',$url.'/'.$id,[
            'headers' => $header,
        ]);
		return back();
	
    }
}
