<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Redirect;
use Auth;

class AuthController extends Controller
{
    
    // public function __construct()
    // {
    //     $this->middleware('gm');
    // }

    public function login(Request $request)
    {
        $client = new Client();

        $url = config('constants.url.gateway').'login';
        //dd($url);

        //dd($client);

        $bodyApiV2 = ['email' => $request->input('user_email'),
            'password' => $request->input('user_password')];

        $responseData = $client->request('POST',$url,['json' =>  $bodyApiV2]);

        $responseData = json_decode($responseData->getBody()->getContents(),true);
        //dd($responseData);

        if ($responseData['StatusCode'] == 200) {
            if($responseData['Data']['role'] == 2){
                Session::put('app_token', $responseData['Data']['app_token']);
            }
            Session::put('admin_id', $responseData['Data']['admin_id']);
            Session::put('role', $responseData['Data']['role']);
            Session::put('name', $responseData['Data']['name']);
            Session::put('email', $responseData['Data']['email']);
            // Session::put('user_phone_number', $responseData['Data']['user_phone_number']);
            // Session::put('user_image', $responseData['Data']['user_image']);
			// Session::put('organization', $responseData['Data']['organisasi']);
            // Session::put('job_title', $responseData['Data']['job_title']);
            Session::put('token', $responseData['Token']);
            Session::put('language_code', 'RUWT-EN');
            Session::put('session_code', 'EN');

            //dd($request);
            $user_role = Session::get('role');
            //dd($user_role);
            if($user_role == 1){
                return redirect('/master');
            }elseif($user_role == 2){
                return redirect('/admin');
            }else{
                return "403 Access denied";
            }
        }else{
            return back()->with('message',' Error Email and/or Password wrong');
        }
        
    }

    
}
