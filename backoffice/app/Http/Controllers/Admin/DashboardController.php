<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class DashboardController extends Controller
{
    public function master(){
        $user = Session::all();
        //dd($user);
        return view('superadmin.index',compact('user'));
    }
}
