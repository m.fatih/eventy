<?php

namespace App\Http\Middleware;

use Closure;

class GuestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user= Session::all();
        if($user){
            if($user->role == 1){
                return redirect('master');
            }elseif($user->role == 2){
                return redirect('admin');
            }
        }
        return $next($request);
    }
}
