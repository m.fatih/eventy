<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class MasterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_role = Session::get('user_role');
        if($user_role == 1){
            return $next($request);
        }
            return redirect('/login')->with('message','ACCESS DENIED');
    }
}
