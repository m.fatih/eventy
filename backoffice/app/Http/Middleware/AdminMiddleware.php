<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_role = Session::get('role');
        if($user_role == 1 || $user_role == 2){
            return $next($request);
        }
            return redirect('/login')->with('message','403 Error | You have not admin access');
    }
}
